<?php

class TkiInvite extends Page {
	/* ---- Static variables ---- */
	public static $db = array(
		'FromEmail' => 'Varchar(128)',
		'EmailSubject' => 'Varchar(128)',
		'MaxRecipients' => 'Int',
		'Disclaimer' => 'HTMLText',
		'Greeting' => 'Varchar(128)',
		'Header' => 'HTMLText',
		'Footer' => 'HTMLText',
		'Legal' => 'HTMLText',
		'BodyStyles' => 'Text',
		'BodyLink' => 'Varchar(128)',
		'BodyLinkVisited' => 'Varchar(128)',
		'BodyLinkHover' => 'Varchar(128)',
		'ContainerStyles' => 'Text',
		'ContainerLink' => 'Varchar(128)',
		'ContainerLinkVisited' => 'Varchar(128)',
		'ContainerLinkHover' => 'Varchar(128)',
		'PlainText' => 'Text',
		'ActionLinkText' => 'Varchar(80)',
		'ActionLinkBaseUrl' => 'Varchar(255)'
	);
	public static $defaults = array(
		'MaxRecipients' => 3
	);
	public static $has_one = array();
	
	public static $has_many = array();
	
	public static $many_many = array();

	public static $belongs_many_many = array();

	public static $singular_name = 'Invite';
	public static $plural_name = 'Invite';
	
	public static $default_from_email = null;
		
		// Email and its properties
	protected $email = null;

		// Shortcodes and configured defaults
	protected static $default_shortcodes = array(
		'sendername' => '',
		'senderemail' => '',
		'recipientname' => '',
		'recipientemail' => '',
		'greeting' => '',
		'custommessage' => '',
		'actionlinktext' => '',
		'actionlinkurl' => ''
	);
	
	/* ---- Instance variables ---- */
	protected $instanceShortcodes = array();
	protected $cachedShortcodes = null;
	protected $cachedBracketedShortcodes = null;
	
	/* ---- Static methods ---- */
	public static function get_frontend_fields() {
		$fields = new Fieldset(
			new HeaderField("InviteHeader",_t("TkiInvite.HEADER",'Customise your invitation'),5),
			new TextField('Greeting',_t('TkiInvite.GREETING','Greeting'),'',60),
			new TextareaField('CustomMessage',_t('TkiInvite.MESSAGE','Message'),8,30)
		);
		return $fields;	
	}
	
	public static function set_default_shortcode($code,$val) {
		$code = (string) $code;
		self::$default_shortcodes[$code] = (string) $val;
	}
	
	public static function is_default_shortcode($code) {
		return in_array($code,self::$default_shortcodes);
	}
	
	public static function unset_default_shortcode($code) {
		if(self::is_default_shortcode($code)) unset(self::$default_shortcodes[$code]);
	}
	
	public static function get_default_shortcodes() {
		return self::$default_shortcodes;
	}
	
	/* ---- Instance methods ---- */
	public function setShortcode($code,$val) {
		$code = (string) $code;
		$this->instanceShortcodes[$code] = (string) $val;	
	}
	
	public function getShortcode($code,$cached=false) {
		$code = (string) $code;
		if($cached && $this->cachedShortcodes) {
			$shortcodes = $this->cachedShortcodes;
		} else {
			$shortcodes = $this->getMergedShortcodes();
		}
		
		return (isset($shortcodes[$code])) ? $shortcodes[$code] : null;
	}
	
	public function getInstanceShortcodes() {
		return $this->instanceShortcodes;
	}
	
	public function getMergedShortcodes($cached=false) {
		if($cached && $this->cachedShortcodes) return $this->cachedShortcodes;
		$this->cachedShortcodes = array_merge(self::$default_shortcodes,$this->instanceShortcodes);
		return $this->cachedShortcodes;
	}
	
	/* Takes map of codes and replacements 
	 * and replaces in content provided.
	 */
	public function parseShortcodes($content) {
		if(empty($content)) return $content;
			// Run regular shortcode parser
		$content = ShortcodeParser::get('default')->parse($content);
			// Show shortcodes in place
		if(!empty($_GET['showcodes'])) return $content;
			// Replace shortcodes
		$shortcodes = $this->getMergedShortcodes(false);
		$codes = array_keys($shortcodes);
		
		foreach($codes as $k => &$v) {
			$v = '['. $v .']';
		}
		$replacements = array_values($shortcodes);

		if(!empty($codes)) {
			$content = str_replace($codes,$replacements,$content);
		}
		return $content;
	}
	public function makeActionLink() {
		$this->setShortcode('actionlinktext',$this->ActionLinkText);
		$this->setShortcode('actionlinkurl',$this->ActionLinkBaseUrl);
		// To do: add query vars
	}
	
	public function EmailSubject() {
		return $this->parseShortcodes($this->EmailSubject);
	}
	public function Disclaimer() {
		return $this->parseShortcodes($this->Disclaimer);
	}
	public function Header() {
		return $this->parseShortcodes($this->Header);
	}
	public function Content() {
		return $this->parseShortcodes($this->Content);
	}
	public function Footer() {
		return $this->parseShortcodes($this->Footer);
	}
	public function Legal() {
		return $this->parseShortcodes($this->Legal);
	}
	public function PlainText() {
		return $this->parseShortcodes($this->PlainText);
	}
	public function InviteStyles() {
		return $this->renderWith('TkiInvite_styles');
	}
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		
		$fields->removeFieldsFromTab('Root.Content',array(
			'GoogleSitemap',
			'Images',
			'Content'
		));
		HtmlEditorConfig::get('TkiInvite')->setOption('content_css',$this->AbsoluteLink('styles'));
		//Debug::dump($this->AbsoluteLink('styles'));
		//Debug::dump(HtmlEditorConfig::get_available_configs_map());
		//	
			// Settings
		$settingsFields = array(
			new EmailField('FromEmail',_t('TkiInvite.FROMEMAIL','Email in the from field')),
			new TextField('EmailSubject',_t('TkiInvite.EMAILSUBJECT','Email subject')),
			new NumericField('MaxRecipients',_t('TkiInvite.MAXRECIPIENTS','Maximum recipients on form')),
			new TextareaField('BodyStyles',_t('TkiInvite.BODYSTYLES','Body styles')),
			new TextField('BodyLink',_t('TkiInvite.BODYLINK','Body link styles')),
			new TextField('BodyLinkVisited',_t('TkiInvite.BODYLINKVISITED','Body visited link styles')),
			new TextField('BodyLinkHover',_t('TkiInvite.BODYLINKHOVER','Body link hover styles')),
			new TextareaField('ContainerStyles',_t('TkiInvite.CONTAINER','Container styles')),
			new TextField('ContainerLink',_t('TkiInvite.CONTAINERLINK','Container link styles')),
			new TextField('ContainerLinkVisited',_t('TkiInvite.CONTAINERLINKVISITED','Container visited link styles')),
			new TextField('ContainerLinkHover',_t('TkiInvite.CONTAINERLINKHOVER','Container link hover styles'))
		);
		$disclaimer = new CustomConfigHtmlEditorField('TkiInvite','Disclaimer',_t('TkiInvite.DISCLAIMER','Email disclaimer'));
		
		$settingsFields[] = $disclaimer;
		
		$fields->addFieldsToTab('Root.Content.Settings',$settingsFields);
		
			// Header
		$headerFields = array(
			new CustomConfigHtmlEditorField('TkiInvite','Header',_t('TkiInvite.HEADER','Invite header'))
		);
		$fields->addFieldsToTab('Root.Content.Header',$headerFields);
		
			// Content
		$contentFields = array(
			new TextField('Greeting',_t('TkiInvite.GREETING','Greeting')),
			new TextField('ActionLinkText',_t('TkiInvite.ACTIONLINKTEXT','Text for action button')),
			new TextField('ActionLinkBaseUrl',_t('TkiInvite.ACTIONLINKURL','Base URL for action button')),
			new CustomConfigHtmlEditorField('TkiInvite','Content',_t('TkiInvite.CONTENT','Invite content'))
		);
		$fields->addFieldsToTab('Root.Content.Content',$contentFields);
		
			// Footer
		$footer = new CustomConfigHtmlEditorField('TkiInvite','Footer',_t('TkiInvite.FOOTER','Invite footer'));
		
		$fields->addFieldToTab('Root.Content.Footer',$footer);
		
			// Legal
		$legal = new CustomConfigHtmlEditorField('TkiInvite','Legal',_t('TkiInvite.LEGAL','Copyright and legal information'));
		
		$fields->addFieldToTab('Root.Content.Legal',$legal);
	
		// Legal
		$plain = new TextareaField('PlainText',_t('TkiInvite.PLAINTEXT','Enter plain text version'),30);
		
		$fields->addFieldToTab('Root.Content.PlainText',$plain);
	
		
		return $fields;
	}

	public function send() {
			// Configure email instance
		$email = new TkiEmail();
		$email->setTemplate('TkiInvite');
		$email->setFrom($this->FromEmail);
		$email->setSubject(htmlentities($this->EmailSubject()));
		$email->setPlainTextBody($this->PlainText());
		
		
		$email->populateTemplate(
			array(
				'BodyStyles' => $this->BodyStyles,
				'BodyLink' => $this->BodyLink,
				'BodyLinkVisited' => $this->BodyLinkVisited,
				'BodyLinkHover' => $this->BodyLinkHover,
				'ContainerStyles' => $this->ContainerStyles,
				'ContainerLink' => $this->ContainerLink,
				'ContainerLinkVisited' => $this->ContainerLinkVisited,
				'ContainerLinkHover' => $this->ContainerLinkHover,
				'InviteStyles' => $this->InviteStyles(),
				'Disclaimer' => $this->Disclaimer(),
				'Header' => $this->Header(),
				'Content' => $this->Content(),
				'Footer' => $this->Footer(),
				'Legal' => $this->Legal()
			)
		);
		
		$email->replyTo($this->getShortcode('senderemail'));
		$recipient = $this->getShortcode('recipientemail');
		if(empty($recipient)) return false;
		
		$email->setTo($recipient);
		
		$result = $email->send();
		return $result;
	}
	
	
	
}

class TkiInvite_Controller extends Page_Controller {
	/* ---- Static variables ---- */
	public static $allowed_actions = array('styles');
	
	/* ---- Instance variables ---- */
	
	
	/* ---- Static methods ---- */
	
	
	/* ---- Instance methods ---- */
	public function init() {
		parent::init();
		
		Requirements::clear();
	
	}
	
	
}

?>