<?php
/**
  * @author Todd [at] tiraki.com
  * 
  */
class TkiInviteForm extends TkiForm {

	function __construct($controller, $name) {
		
		$formData = (array) TkiForm::get_form_data_from_session($name);
		
		$fields = TkiInviteSender::get_frontend_fields();
		
		$invite = $controller->TkiInvite();
		
		$fields->push(new HeaderField("RecipientsHeader",_t("TkiInviteForm.RECIPIENTSHEADER","Recipient(s)"),5));
		$recipients = new CompositeField();
		$recipients->setID('Recipients');
		
		$maxRecipients = $invite->MaxRecipients;
		if(!$maxRecipients) $maxRecipients = 1;
		$i = 0;
		for($i = 1; $i <= $maxRecipients; ++$i) {
			$recipients->push(new CompositeField(
				new TextField("RecipientName-$i",_t("TkiInviteForm.RECIPIENTNAME$i",'Name'),'',60),
				new EmailField("RecipientEmail-$i",_t("TkiInviteForm.RECIPIENTNAME$i",'Email'),'',60)
			));
		}
		
		$fields->push($recipients);
		
		$inviteFields = $invite->get_frontend_fields();
		if(!empty($inviteFields)) $fields->merge($inviteFields);
		
		$requiredFields = array('Name','Email','RecipientName-1','RecipientEmail-1');
		
		// Specific validation logic
		$requiredFields[] = array(
			'js' => '',
			'php' => '$recipients = TkiInviteForm::get_recipients($this->form);
				foreach($recipients as $fieldGroup) {
					$fieldNames = array_keys($fieldGroup);
					$fieldValues = array_values($fieldGroup);
					if(!empty($fieldValues[0])) $this->requireField($fieldNames[1],$data);
					if(!empty($fieldValues[1])) $this->requireField($fieldNames[0],$data);
				}
			'
		);
		$validator = new TkiRequiredFields($requiredFields);
		
		
		$actions = new FieldSet(
			new FormAction('submit', _t('TkiInviteForm.SUBMIT','Send'))
		);
		
		parent::__construct($controller, $name, $fields, $actions, $validator);
		

	}
	
	function get_recipients($form) {
		$recipients = array();
		$fieldGroups = $form->fields()->fieldByName("Recipients")->getChildren();
		if($fieldGroups) {
			foreach($fieldGroups as $k=>$group) {
				if(is_object($group)) {
					$fields = $group->getChildren()->toArray();
					$recipients[] = array(
						$fields[0]->Name() => $fields[0]->Value(),
						$fields[1]->Name() => $fields[1]->Value()
					);
				}
			}
		}
		return $recipients;
	}
	
	/**
	 * Standalone signup form
	 */
	function submit($data, $form, $request) {
		$formData = $form->getData();
		//Debug::dump($formData);
			// Prepare invite
		$invite = $form->controller->TkiInvite();
		if(!$invite || !$invite->exists()) {
			$this->sessionMessage(_t($this->class .'.NOINVITE','No invite found!'), 'bad');
			Director::redirectBack();
			return false;
		}
		
			// Configure invite
		if(empty($formData['Name']) || empty($formData['Email'])) {
			$this->sessionMessage(_t($this->class .'.NOSENDER','Sender name or email not found!'), 'bad');
			Director::redirectBack();
			return false;
		} 
		$senderName = $formData['Name'];
		$senderEmail = $formData['Email'];
		
		$invite->setShortcode('sendername',$senderName);
		$invite->setShortcode('senderemail',$senderEmail);
		
		if(!empty($formData['Greeting'])) $invite->setShortcode('greeting',$formData['Greeting']);
		if(!empty($formData['CustomMessage'])) $invite->setShortcode('custommessage',$formData['CustomMessage']);
		
		
			// Customise and send to each recipient
		$invitesSent = 0;
		$recipients = self::get_recipients($form);
		foreach($recipients as $recipient) {
			list($recipientName,$recipientEmail) = array_values($recipient);
			if(!empty($recipientName) && !empty($recipientEmail)) {
				$invite->setShortcode('recipientname',$recipientName);
				$invite->setShortcode('recipientemail',$recipientEmail);
				$res = $invite->send();
				if($res) ++$invitesSent;
			}
		}
		
		$sender = DataObject::get_one('TkiInviteSender',"\"Email\"='$senderEmail'");
		if(!$sender) {
			$sender = new TkiInviteSender();
			$sender->setField('Name',$senderName);
			$sender->setField('Email',$senderEmail);
			$sender->setField('NumSent',$invitesSent);
			if($invitesSent > 0) {
				$sender->setField('FirstSent',SS_Datetime::now()->Rfc2822());
			}
			
		} else {
			if($invitesSent > 0) {
				$numSent = (int) $sender->getField('NumSent') + $invitesSent;
				$sender->setField('NumSent',$numSent);
				$sender->setField('LastSent',SS_Datetime::now()->Rfc2822());
			}

		}
		
		$result = $sender->write();

		$url = $this->controller->Link() .'completed';
		
		return $this->finishSubmit($result,$url);
	}

}
