<?php

class TkiInviteFormPage extends Page {
	/* ---- Static variables ---- */
	public static $db = array(
		'CompletedMessage' => 'HTMLText'
	);	// To do: get user selectable fields to include on form?

	public static $has_one = array(
		'TkiInvite' => 'TkiInvite'
	);
	
	public static $has_many = array();
	
	public static $many_many = array();

	public static $belongs_many_many = array();

	public static $singular_name = 'Invite Form Page';
	public static $plural_name = 'Invite Form Pages';
	
	/* ---- Instance variables ---- */
	
	
	/* ---- Static methods ---- */
		
	/* ---- Instance methods ---- */
	function getCMSFields() {
		$fields = parent::getCMSFields();
			// Choose single invite
		$invite = new HasOneComplexTableField(
			$this,
			'TkiInvite',
			'TkiInvite',
			array('Title'=>'Title')
		);
		$invite->setPermissions(array());
		$fields->addFieldToTab('Root.Content.Invite',$invite);
		
			// Completion message
		$completed = new HtmlEditorField('CompletedMessage',_t('TkiInviteFormPage.COMPLETEDMESSAGE','Message on completion'));
		
		$fields->addFieldToTab('Root.Content.Completed',$completed);
		
		return $fields;
	}
}

class TkiInviteFormPage_Controller extends Page_Controller {
	/* ---- Static variables ---- */
	public static $allowed_actions = array();
	
	/* ---- Instance variables ---- */
	
	
	/* ---- Static methods ---- */
	public static function get_form_fields() {
		
	}
	
	/* ---- Instance methods ---- */
	public function init() {
		
		parent::init();
		
		Session::set('BackURL',$this->Link());
	}
	
	/* ---- Template methods ---- */
	public function InviteForm() {
		$invite = $this->TkiInvite();
		if(!$invite || !$invite->exists()) {
			return _t('TkiInviteForm.NOINVITE','Sorry, no invite is configured!');
		} else {
			return new TkiInviteForm($this,'InviteForm');
		}

	}
	
	/**
	 * This action handles rendering the "completed" message,
	 * which is customisable by editing the TkiInviteFormCompleted.ss
	 * template.
	 *
	 * @return ViewableData
	 */
	function completed() {
		$backLink = $this->getBackUrl();
		
		return $this->customise(array(
			'Content' => $this->customise(
				array(
					'Link' => $backLink
				))->renderWith('TkiInviteFormCompleted'),
			'InviteForm' => ' ',
		));
	}
}

?>