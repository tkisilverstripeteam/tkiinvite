<?php

class TkiInviteSender extends DataObject {
	/* ---- Static variables ---- */
	public static $db = array(
		'Name' => 'Varchar',
		'Email' => 'Varchar(64)',
		'NumSent' => 'Int',
		'FirstSent' => 'SS_Datetime',
		'LastSent' => 'SS_Datetime'
	);
	
	public static $has_one = array(
		'Member' => 'Member'
	);

	public static $has_many = array();

	public static $many_many = array();

	public static $belongs_many_many = array();
	
	public static $singular_name = 'Invite Sender';
	public static $plural_name = 'Invite Senders';
	
	/* ---- Instance variables ---- */
	
	/* ---- Static methods ---- */
	public static function getCMSFields_forPopup() {
		return new FieldSet(
			new ReadonlyField('Name',_t('TkiInviteSender.NAME','Name')),
			new ReadonlyField('Email',_t('TkiInviteSender.EMAIL','Email')),
			new ReadonlyField('NumSent',_t('TkiInviteSender.NUMSENT','Total emails sent')),
			new ReadonlyField('FirstSent',_t('TkiInviteSender.FIRSTSENT','Date of first email')),
			new ReadonlyField('FirstSent',_t('TkiInviteSender.LASTSENT','Date of latest email'))	
		);
	}
	
	public static function get_frontend_fields() {
		$fields = new Fieldset(
			new HeaderField("SenderHeader",_t("TkiInviteSender.HEADER",'Sender'),5),
			new TextField('Name',_t('TkiInviteSender.NAME','Name'),'',60),
			new EmailField('Email',_t('TkiInviteSender.EMAIL','Email'),'',60)
		);
		return $fields;		
	}
	
	/* ---- Instance methods ---- */

	public function canView($member = null) {
		return $this->Page()->canView();
	}

	public function canEdit($member = null) {
		return $this->Page()->canEdit();
	}

	public function canDelete($member = null) {
		return $this->Page()->canEdit();
	}

	public function canCreate($member = null) {
		return $this->Page()->canEdit();
	}

}

?>